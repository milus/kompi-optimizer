package AST

object Priority {
  val binary = Map("lambda" -> 1,
    "or" -> 2,
    "and" -> 3,
    "is" -> 8, "<" -> 8, ">" -> 8, ">=" -> 8, "<=" -> 8, "==" -> 8, "!=" -> 8,
    "+" -> 9, "-" -> 9,
    "*" -> 10, "/" -> 10, "%" -> 10)

  val unary = Map("not" -> 4,
    "+" -> 12, "-" -> 12)
}

// sealed trait Node would be also OK
sealed abstract class Node {
  def makeProgramString = "error: toString not implemented"

  val indent = " " * 4
}

case class IntNum(value: Int) extends Node {
  override def makeProgramString = value.toString
}

case class FloatNum(value: Double) extends Node {
  override def makeProgramString = value.toString
}

case class StringConst(value: String) extends Node {
  override def makeProgramString = value
}

case class TrueConst() extends Node {
  override def makeProgramString = "True"
}

case class FalseConst() extends Node {
  override def makeProgramString = "False"
}

case class Variable(name: String) extends Node {
  override def makeProgramString = name
}

case class Unary(op: String, expr: Node) extends Node {

  override def makeProgramString = {
    var str = expr.makeProgramString
    expr match {
      case e@BinExpr(_, _, _) => if (Priority.binary(e.op) <= Priority.unary(op)) {
        str = "(" + str + ")"
      }
      case e@Unary(_, _) => if (Priority.unary(e.op) <= Priority.unary(op)) {
        str = "(" + str + ")"
      }
      case _ =>
    }
    op + " " + str
  }

}

case class BinExpr(op: String, left: Node, right: Node) extends Node {

  override def makeProgramString = {
    var leftStr = left.makeProgramString
    var rightStr = right.makeProgramString
    left match {
      case l@(_: BinExpr) => if (Priority.binary(l.op) < Priority.binary(op)) {
        leftStr = "(" + leftStr + ")"
      }
      case l@(_: Unary) => if (Priority.unary(l.op) < Priority.binary(op)) {
        leftStr = "(" + leftStr + ")"
      }
      case _ =>
    }
    right match {
      case r@BinExpr(_, _, _) => if (Priority.binary(r.op) < Priority.binary(op)) {
        rightStr = "(" + rightStr + ")"
      }
      case r@Unary(_, _) => if (Priority.unary(r.op) < Priority.binary(op)) {
        rightStr = "(" + rightStr + ")"
      }
      case _ =>
    }
    leftStr + " " + op + " " + rightStr
  }

  override def equals(that: scala.Any): Boolean = {
    def uncommutativeEquals(that: BinExpr) = this.left == that.left && this.right == that.right
    def commutativeEquals(that: BinExpr) = uncommutativeEquals(that) || (this.left == that.right && this.right == that.left)

    that match {
      case that:BinExpr if this.op == that.op => that.op match {
        case "+" => commutativeEquals(that)
        case "*" => commutativeEquals(that)
        case "or" => commutativeEquals(that)
        case "and" => commutativeEquals(that)
        case default => uncommutativeEquals(that)
      }
      case default => false
    }
  }
}

case class IfElseExpr(cond: Node, left: Node, right: Node) extends Node {
  override def makeProgramString = left.makeProgramString + " if " + cond.makeProgramString + " else " + right.makeProgramString
}

case class Assignment(left: Node, right: Node) extends Node {
  override def makeProgramString = left.makeProgramString + " = " + right.makeProgramString
}

case class Subscription(expr: Node, sub: Node) extends Node {
  override def makeProgramString = expr.makeProgramString + "[" + sub.makeProgramString + "]"
}

case class KeyDatum(key: Node, value: Node) extends Node {
  override def makeProgramString = key.makeProgramString + ": " + value.makeProgramString

  override def equals(that: scala.Any): Boolean = that match {
    case that: KeyDatum => this.key == that.key
    case other => false
  }

  override def hashCode(): Int = key.hashCode()
}

case class GetAttr(expr: Node, attr: String) extends Node {
  override def makeProgramString = expr.makeProgramString + "." + attr
}

case class IfInstr(cond: Node, left: Node) extends Node {
  override def makeProgramString = {
    var str = "if " + cond.makeProgramString + ":\n"
    str += left.makeProgramString.replaceAll("(?m)^", indent)
    str
  }
}

case class IfElseInstr(cond: Node, left: Node, right: Node) extends Node {
  override def makeProgramString = {
    var str = "if " + cond.makeProgramString + ":\n"
    str += left.makeProgramString.replaceAll("(?m)^", indent)
    str += "\nelse:\n"
    str += right.makeProgramString.replaceAll("(?m)^", indent)
    str
  }
}

case class WhileInstr(cond: Node, body: Node) extends Node {
  override def makeProgramString = {
    "while " + cond.makeProgramString + ":\n" + body.makeProgramString.replaceAll("(?m)^", indent)
  }
}

case class InputInstr() extends Node {
  override def makeProgramString = "input()"
}

case class ReturnInstr(expr: Node) extends Node {
  override def makeProgramString = "return " + expr.makeProgramString
}

case class PrintInstr(expr: Node) extends Node {
  override def makeProgramString = "print " + expr.makeProgramString
}

case class FunCall(name: Node, args_list: Node) extends Node {

  override def makeProgramString = {
    args_list match {
      case NodeList(list) => name.makeProgramString + "(" + list.map(_.makeProgramString).mkString("", ",", "") + ")"
      case _ => name.makeProgramString + "(" + args_list.makeProgramString + ")"
    }
  }
}

case class FunDef(name: String, formal_args: Node, body: Node) extends Node {
  override def makeProgramString = {
    var str = "\ndef " + name + "(" + formal_args.makeProgramString + "):\n"
    str += body.makeProgramString.replaceAll("(?m)^", indent) + "\n"
    str
  }
}

case class LambdaDef(formal_args: Node, body: Node) extends Node {
  override def makeProgramString = "lambda " + formal_args.makeProgramString + ": " + body.makeProgramString
}

case class ClassDef(name: String, inherit_list: Node, suite: Node) extends Node {
  override def makeProgramString = {
    val str = "\nclass " + name
    var inheritStr = ""
    val suiteStr = ":\n" + suite.makeProgramString.replaceAll("(?m)^", indent)
    inherit_list match {
      case NodeList(x) => if (x.length > 0) inheritStr = "(" + x.map(_.makeProgramString).mkString("", ",", "") + ")"
      case _ =>
    }
    str + inheritStr + suiteStr
  }
}

case class NodeList(list: List[Node]) extends Node {
  override def makeProgramString = {
    list.map(_.makeProgramString).mkString("", "\n", "")
  }
}

case class KeyDatumList(list: List[KeyDatum]) extends Node {
  override def makeProgramString = list.map(_.makeProgramString).mkString("{", ",", "}")
}

case class IdList(list: List[Variable]) extends Node {
  override def makeProgramString = list.map(_.makeProgramString).mkString("", ",", "")
}

case class ElemList(list: List[Node]) extends Node {
  override def makeProgramString = list.map(_.makeProgramString).mkString("[", ",", "]")
}

case class Tuple(list: List[Node]) extends Node {
  override def makeProgramString = if (list.length == 0) "()"
  else if (list.length == 1) "(" + list(0).makeProgramString + ",)"
  else list.map(_.makeProgramString).mkString("(", ",", ")")
}

case class Empty() extends Node {
  override def makeProgramString = ""
}
        